# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

import gzip
import re


class CorpusReader:

    # RegEx to match newlines #
    no_newline = re.compile(r"\r?\n|\r")

    def __init__(self, list_of_files, fields, is_compressed):
        self.is_compressed = is_compressed
        self.files = list_of_files
        self.identifiers = ["PMID"]
        for field in fields:
            self.identifiers.append(field.ljust(4))

    def get_identifiers(self):
        return [x.rstrip() for x in self.identifiers]

    def get_info(self):
        self.identifiers = set(self.identifiers)
        # # # If there are multiple files to be read, this loop goes through each one # # #
        for file in self.files:

            # Printing what file is being read #
            aux_file = file.split("/")
            print("\nLeitura do Corpus de:", aux_file[len(aux_file) - 1])

            if self.is_compressed:
                f_read = gzip.open(file, "rt")
            else:
                f_read = open(file)
            output_tuple = self.reader(f_read)
            while output_tuple:
                yield output_tuple
                output_tuple = self.reader(f_read)
            f_read.close()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Method reader(self, file) returns a list[PMID, TITLE],  #
    # and if it reaches the end of the file, returns None     #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    def reader(self, f_read):

        # # # Declaring variables to store relevant information # # #
        line = f_read.readline()  # first line
        output_list = []

        while line.rstrip() != '':

            if line[:4] in self.identifiers:
                aux_string = line[6:]
                line = f_read.readline()
                while line[:4].rstrip() == "":
                    aux_string += " " + line[6:]
                    line = f_read.readline()
                aux_string = self.no_newline.sub("", aux_string)  # remove any newline
                output_list.append(aux_string)
            else:
                line = f_read.readline()

        if output_list:
            return output_list
        else:
            return None
