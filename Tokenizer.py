# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

import re
import Stemmer


class Tokenizer:

    def __init__(self, type_tokenizer, filename):
        if type_tokenizer == "improved":
            self.stop_words = []
            self.get_stop_words(filename)
            self.stemmer = Stemmer.Stemmer("english")  # gets a stemmer for the English language

    # # # Regex compiled for simple_tokenize and in special cases of improved_tokenize # # #
    less_than_three = re.compile(r'\b\w{1,2}\b')  # matches if a word has less than three characters
    only_alphabet = re.compile(r'[^A-Za-z]')  # matches with everything that is not alphabetic characters

    # # # Regex compiled for improved_tokenize # # #
    # Group 1: Words followed by numbers or special characters, that end on alphabetic characters.
    # Group 2: Words that end on numbers or special characters.
    # Group 3: Words that start with numbers, followed by special characters or other numbers,
    # that end on alphabetic characters.
    # Group 4: Words containing only alphabetic characters with length greater than 3.
    alphanumeric_special = re.compile(r"([A-Za-z]+[\d'.\-]+[A-Za-z]+)|"
                                      r"([A-Za-z]+[\d'.\-]+)|"
                                      r"([0-9]+[A-Za-z'.\-,]+[a-zA-Z0-9'\-]+[a-zA-Z]+)|"
                                      r"([a-zA-Z]{3,})")

    # # # Method to decide which tokenizer method to call # # #
    def tokenize(self, stream, type_tokenizer):
        return getattr(self, type_tokenizer + "_tokenize")(stream)

    # # # Method for a simple tokenizer # # #
    # it uses the method sub() by using the compiled RegEx to substitute everything
    # according to the expression and returns a list of lower cased strings
    def simple_tokenize(self, stream):
        return self.less_than_three.sub('', self.only_alphabet.sub(' ', stream)).lower().split()

    # # # Method for an improved tokenizer # # #
    def improved_tokenize(self, stream):
        tokens_list = ["".join(token) for token in re.findall(self.alphanumeric_special, stream)]  # gets all words
        # that match the RegEx and turns them to a list of tuples, and each tuple has the same number of elements as
        # there are groups in the RegEx. The tuples empty for groups that did not match. Example: 'concerning' is a
        # match of group 3, so the tuple is: ('','','concerning'), where the first two groups didn't match, so are
        # empty strings. After that, joins all matches in a list.

        stemmed_tokens_list = []  # auxiliary list to receive the terms because of the way re.findall
        for token in tokens_list:
            if token.lower() not in self.stop_words:  # gets a token from the tuples and only adds
                # it to the tokenized_list if they are a stop word.
                if token.isupper() and len(token) < 5:  # if a token has all characters in uppercase and has less
                    # than 5 characters, it's stemmed as is
                    stemmed_token = self.stemmer.stemWord(token)
                else:  # else, if a token is not all uppercase, then any uppercase characters are changed to
                    # lowercase, then stemmed. The reason for this is because the Stemmer will stem 'als' to
                    # 'al', but won't stem 'ALS' as it is an acronym. But in the case of words like 'Abnormal'
                    # and 'abnormal', the Stemmer won't stem the one with an uppercase letter.
                    stemmed_token = self.stemmer.stemWord(token.lower())
                stemmed_tokens_list.append(stemmed_token.lower())

        return stemmed_tokens_list  # returns a list of tokens stemmed

    # # # Method to get a set of stop words # # #
    def get_stop_words(self, filename):
        print("Obtendo Stop Words.")
        stop_words = set()
        with open(filename) as file:
            line = file.readline()
            while line:
                stop_words.add(line.rstrip())  # making sure there aren't any whitespaces
                line = file.readline()
        self.stop_words = stop_words
