# Recuperação de Informação
## Professor Doutor Sérgio Guilherme Aleixo de Matos
### Assignment 3 | Ranked Retrieval and Evaluation

## Informations about the student
Helder Paraense   | N.Mec.: 96307

## Requirements
- Python 3
    - PyStemmer (https://pypi.org/project/PyStemmer/) - For execution on Windows Microsoft Visual C++ Build Tools (https://go.microsoft.com/fwlink/?LinkId=691126) is necessary
    - psutil - Install via Pip. For help getting Memory information of the Python Process.

## How to run the program for Indexation
You need to run \_\_init__.py and pass its parameters:

**Structure**:

**WARNING**: It's best to keep the folder organization

usage: \_\_init__.py [-h] [-f F] [-gz {0,1}] [-t {simple,improved}] [-i {1,2,3}]
                   [-fc FC [FC ...]] [-cs CS] [-smp SMP] [-smb SMB]

Script for indexation of terms present in a corpus.

optional arguments:
  -h, --help            show this help message and exit
  
  -f F                  This is the path to the input files
  
  -gz {0,1}             Indicate if files are compressed
  
  -t {simple,improved}  This is the type of tokenizer to utilize
  
  -i {1,2,3}            This is type of indexation:
                        1 - Indexer from Assignment 1
                        2 - Indexer with lnc.ltc indexing schema, without positions
                        3 - Indexer with lnc.ltc indexing schema, with positions
                        
  -fc FC [FC ...]       Which fields of the corpus should be indexed. Examples: 
                        TI
                        TI AB
                        
  -cs CS                Tentative size of each index file in MB. The minimum. Examples: 
                        50.0 - When there are 50 MB of data to write, the program will keep writing until the first 
                        character changes, i.e., if it reaches 50 MB 
                        while there are still index starting with A, it will write to file only when all indexes 
                        starting with A are written to disk. So, it will write when it finds an index starting with B.
                        
  -smp SMP              SPIMI by memory percentage. Example: 
                        10 - blocks are made when 10 percent of free memory that was available in the beginning is 
                        utilized during indexation
                        WARNING: can't be used alongside -smb
                        
  -smb SMB              SPIMI by memory block in MB. Example: 
                        200 - blocks are made when 200 MB of memory are occupied by the Python process
                        WARNING: can't be used alongside -smp

**Example of running the program with default arguments:**

python \_\_init__.py

**Example of running the program indicating a folder and the simple tokenizer as arguments:**

python \_\_init__.py -gz 0 -f files/ -t simple

**Example of running the program indicating the second indexer, that the SPIMI memory block is 600MB, 
and that index files should be 40 MB or more as arguments:**

python \_\_init__.py -i 2 -smb 600 -cs 40

**Example of running the program specifying a folder, with third indexer, that the SPIMI memory 
is 10% of available memory, and that index files should be 70 MB or more as arguments:**

python \_\_init__.py -f files/ -i 3 -smp 10 -cs 70

## How to run the program for Retrieval
You need to run search.py and pass its parameters:

**Structure**:

**WARNING**: It's best to keep the folder organization

usage: search.py [-h] [-f F] [-fr FR] [-wt WT] [-bm25 {0,1}] [-k1 K1] [-b B]
                 [-bmt BMT] [-sw SW] [-fi FI]

Script for retrieval of documents present in a corpus

optional arguments:
  -h, --help   show this help message and exit
  
  -f F         This is the path to the queries file
  
  -fr FR       This is the path to the queries relevance file
  
  -wt WT       This is the weight threshold for the document frequency.
               If the weight of a term in a document is below this value,
               and the term idf is not high enough, it is ignored.
               
  -bm25 {0,1}  This is for bm25 ranking. The default is 0:
               0 - Indexed for tf-idf
               1 - Indexed for BM25
               
  -k1 K1       This is the k parameter for bm25 ranking.
               The default is 1.2, and should be a positive real number.
               
  -b B         This is the b parameter for bm25 ranking.
               The default is 0.75, and the value should be between 0.0 and 1.0.
               0.0 disables normalization, and 1.0 normalizes completely.
               
  -bmt BMT     This is the documents threshold for the BM25. This is used as a stop criterium, and indicates 
               how many documents are going to have their TF calculated before stopping.
               100 is the default, and it shouldn't be lower than 50 so the test with 50 retrieved results won't 
               have less than that.
               If 0 is given for this argument, the threshold will be ignored.
               
  -sw SW       This is the path to the stop words file
  
  -fi FI       This is the path to the indexes folder
  
**Example of running the program with default arguments:**

python search.py

**Example indicating the directory for the BM25 indexes, and informing that the scoring should be done with BM25.
The default parameters are explained above in the helper text:**

python search.py -bm25 1 -fi outputfiles/index_tknimproved_bm25/

**Same as above, but with different k1 and b:**

python search.py -bm25 1 -fi outputfiles/index_tknimproved_bm25/ -k1 1.75 -b 1.0

**For more information, use the helper:**

python search.py -h
