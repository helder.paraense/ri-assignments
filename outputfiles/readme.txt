# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

This folder is for the index files created by the Indexer module or any other sort of output.
Indexes are inside folders depending on the tokenizer method chosen when running __init__.py.
All index files from a previous execution are deleted when a new indexation is made.