# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #
import itertools
import Stemmer
import re
from calculations import calculate_bm25_tf
from Tokenizer import Tokenizer
from os import listdir
from collections import OrderedDict


class RankedRetriever:

    def __init__(self, index_folder, stop_words_file, doc_lengths_file=None, k=None, b=None):
        self.index_dict = {}
        self.index_folder = index_folder
        self.get_index_files()
        self.bm25 = False
        if doc_lengths_file:
            self.bm25 = True
            self.doc_lengths_file = doc_lengths_file
            self.doc_lengths_list = []
            self.average_doc_length = 0.0
            self.bm25_k1 = k
            self.bm25_b = b
            self.get_doc_lengths()
        self.token_o = Tokenizer("improved", stop_words_file)
        self.stemmer = self.token_o.stemmer

    def retrieve(self, query, num_results, threshold):
        ranked_docs = {}
        docs_score = {}
        term_postings = OrderedDict()
        query_idfs = []
        terms_list = self.token_o.improved_tokenize(query)
        count = 0
        num_query_terms = len(terms_list)

        for query_term in terms_list:

            index_dictionary = self.get_index_dictionary(query_term)

            stemmed_term = self.stem(query_term)

            if stemmed_term not in index_dictionary:
                query_idfs.append(0.0)
                count += 1
                continue

            print("Index:", stemmed_term, "IDF:", index_dictionary[stemmed_term].split(";", 1)[0])
            aux = index_dictionary[stemmed_term].split(";", 1)
            idf = float(aux[0])
            postings = aux[1]
            query_idfs.append(idf)
            postings = postings.split(";")
            postings = sorted(postings, key=lambda x: x.split(":")[1], reverse=True)
            term_postings[stemmed_term] = postings

            del aux
            del index_dictionary
            count += 1

        idf_threshold = round(sum(query_idfs) / len([q for q in query_idfs if q != 0]), 3)
        print("IDF Threshold (Mean):", idf_threshold)

        count = 0
        for term in term_postings:
            for p in term_postings[term]:

                if query_idfs[count] == 0:
                    count += 1

                doc_id = int(p.split(":")[0])

                if self.bm25:
                    tf = float(p.split(":")[1])
                    dl = self.doc_lengths_list[doc_id-1]
                    tf_bm25 = calculate_bm25_tf(tf, self.bm25_k1, self.bm25_b, round(dl / self.average_doc_length))

                    if doc_id not in docs_score:
                        docs_score[doc_id] = [0.0] * num_query_terms
                    if threshold != 0:
                        if len(docs_score) > threshold and query_idfs[count] < idf_threshold:
                            break

                    tf = tf_bm25

                else:
                    weight_td = float(p.split(":")[1])

                    # Sometimes, there are outliers. One example is the document with Title: All Too Human. This
                    # document has no Abstract, so there are only three possible tokens, but two of them (All,
                    # Too) are Stop Words. In this case, the Term Frequency for Human in this document is 1.0 after
                    # normalization. There isn't useful information to be retrieved for documents such as this one,
                    # so all documents that have Term Frequency equal to 1.0 in the TF-IDF case are ignored.
                    if round(float(weight_td), 1) == 1.0:
                        continue

                    if doc_id not in docs_score:
                        docs_score[doc_id] = [0.0] * num_query_terms
                    if weight_td < threshold and query_idfs[count] < idf_threshold:
                        break

                    tf = weight_td

                score = round(tf * query_idfs[count], 3)
                docs_score[doc_id][count] = score
            count += 1

        for doc in docs_score:
            ranked_docs[doc] = 0
            for w in docs_score[doc]:
                ranked_docs[doc] += w

        ordered_rank = OrderedDict(sorted(ranked_docs.items(), key=lambda x: x[1], reverse=True))
        retrieved_docs_with_score = itertools.islice(ordered_rank.items(), num_results)

        return OrderedDict(retrieved_docs_with_score)

    def get_index_files(self):
        for f in listdir(self.index_folder):
            if f.__contains__("0-9") and not f.__contains__("bm"):
                self.index_dict["0-9"] = self.index_folder + f
            first_chars = [c for c in f.split("_") if c.isupper()]
            for f_char in first_chars:
                self.index_dict[f_char] = self.index_folder + f

    def get_doc_lengths(self):
        with open(self.doc_lengths_file, "r+") as file:
            lines = file.readlines()
            self.doc_lengths_list = [int(l) for l in lines if l[0].isdigit()]
            self.average_doc_length = float(lines[-1].split(" ")[3])

    def get_index_dictionary(self, query_term):
        first_char = query_term[0].upper()
        if first_char not in self.index_dict:
            query_term = re.sub(r"[^a-zA-Z0-9]+", "", query_term)
            first_char = query_term[0]
        file = self.index_dict[first_char]
        file_read = open(file)
        query_lines = file_read.readlines()
        index_dictionary = {term.split(":")[0]: term.split(":")[1] + ";" + postings for term, postings in
                            (line.split(";", 1) for line in query_lines)}
        file_read.close()
        del query_lines
        return index_dictionary

    def stem(self, query_term):
        if query_term.isupper():
            stemmed_term = self.stemmer.stemWord(query_term)
        else:
            stemmed_term = self.stemmer.stemWord(query_term.lower())
        stemmed_term = stemmed_term.lower()
        return stemmed_term
