# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

from CorpusReader import CorpusReader
from Tokenizer import Tokenizer
import spimi
from os import listdir
from os.path import isfile, join
import time
import argparse

stop_words_file = "files/snowball_stopwords_EN.txt"

# Utilizing the ArgumentParser to get the arguments to run the script, as well as defining some default values #
parser = argparse.ArgumentParser(description='Script for indexation of terms present in a corpus',
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-f", default="inputfiles/", type=str, help="This is the path to the input files")
parser.add_argument("-gz", default=0, type=int, choices=[0, 1], help="Indicate if files are compressed")
parser.add_argument("-t", default="improved", type=str, choices=["simple", "improved"],
                    help="This is the type of tokenizer to utilize")
parser.add_argument("-i", default=3, type=int, choices=[1, 2, 3],
                    help="This is type of indexation:\n1 - Indexer from Assignment 1\n2 - Indexer with lnc.ltc "
                         "indexing schema, without positions\n3 - Indexer with lnc.ltc indexing schema, with positions")
parser.add_argument("-bm25", default=0, type=int, choices=[0, 1],
                    help="This is for bm25 ranking. The default is 0:\n0 - Don't index for BM25\n1 - Index for BM25")
parser.add_argument("-fc", default=["TI", "AB"], nargs="+",
                    help="Which fields of the corpus should be indexed. Examples: \nTI\nTI AB")
parser.add_argument("-cs", default=50.0, type=float,
                    help="Tentative size of each index file in MB. Example: \n50.0 - When there are 50 MB of data to"
                         "write, the program will keep writing until the first \ncharacter changes, i.e., "
                         "if it reaches 50 MB while there are still index starting with A, it \nwill write to file "
                         "only when all indexes starting with A are written to disk. So, it will write \nwhen it finds "
                         "an index starting with B")
parser.add_argument("-smp", default=20.0, type=float,
                    help="SPIMI by memory percentage. Example: \n10 - blocks are made when 10 percent of free memory "
                         "that was available in the beginning is \nutilized during indexation\nWARNING: can't be used "
                         "alongside -smb")
parser.add_argument("-smb", type=int,
                    help="SPIMI by memory block in MB. Example: \n200 - blocks are made when 200 MB of memory are "
                         "occupied by the Python process\nWARNING: can't be used alongside -smp")

args = parser.parse_args()  # parsing arguments
corpus_folder = args.f  # gets the folder where the corpus is
is_compressed = args.gz  # gets information about compression
type_tokenizer = args.t  # gets the type of tokenization
type_indexer = args.i  # gets the type of indexation
bm25 = args.bm25  # gets the type of indexation (if bm25)
fields = args.fc  # gets the fields of the corpus to be indexed
chunk_size = args.cs  # gets the minimum size of each index chunk
mem_spimi_perc = args.smp  # gets memory percentage for SPIMI
mem_spimi_block = args.smb  # gets memory block for SPIMI

if mem_spimi_block is None and mem_spimi_perc is None:
    raise MemoryError("Nenhum parametro de memoria foi definido para o SPIMI.")
elif mem_spimi_block and mem_spimi_perc != 20:
    raise MemoryError("Indicar apenas um parametro de memoria. -smp ou -smb usados conjuntamente nao eh possivel.")

bm25 = True if bm25 == 1 else False  # if bm25 received as argument and equal to 1, then bm25 = True

corpus_list = [corpus_folder + f for f in listdir(corpus_folder) if isfile(join(corpus_folder, f))
               and f != "readme.txt"]

print('\nNumero de ficheiros: ', len(corpus_list))  # prints how many files will be read
print('Lista de ficheiros:', str(corpus_list))  # prints the name of the files
print('Compressao:', is_compressed == 1)  # prints the name of the files
print('Tokenizer:', type_tokenizer)  # prints the name of the tokenization method
print('Tipo de Indexer:', type_indexer)  # prints the type of indexation

corpus_obj = CorpusReader(corpus_list, fields, is_compressed)  # object of class CorpusReader that receives the
# corpus files in its constructor
token_obj = Tokenizer(type_tokenizer, stop_words_file)  # object of class Tokenizer, to use its methods such as
# simple_tokenizer()

print('Campos a serem indexados:', corpus_obj.get_identifiers()[1:])  # prints the type of indexation
start_time = time.time()  # object to calculate at the end the running time of indexation

mem_spimi = mem_spimi_perc if mem_spimi_block is None else mem_spimi_block

spimi.spimi_invert(corpus_obj, token_obj, type_tokenizer, type_indexer, mem_spimi, chunk_size, bm25)

# here the time is being modified to be more friendly to the user when printed
elapsed_time = int(time.time() - start_time)
print("Indexação terminada em " + '{:02d}h{:02d}m{:02d}s'
      .format(elapsed_time // 3600, (elapsed_time % 3600 // 60), elapsed_time % 60))
