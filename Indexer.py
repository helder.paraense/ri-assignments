# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

from collections import OrderedDict
from collections import Counter
from datetime import datetime
from os import listdir
from os.path import isfile, join
import calculations
import gc
import contextlib
import heapq
import sys


class Indexer:

    def __init__(self):
        self.index_collection = {}
        self.tf_td_dictionary = {}
        self.terms_frequency = Counter()
        self.positions_dict = {}
        self.counter = 0

    def __del__(self):
        gc.collect()

    # # # Method to sort index_collection # # #
    def sort_index_collection(self, index_collection):
        ordered_dictionary = OrderedDict(sorted(index_collection.items(), key=lambda x: x[0]))  # sort the indexes
        index_collection = ordered_dictionary.copy()
        ordered_dictionary.clear()
        return index_collection

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Method to transform tokens into terms with the Counter  #
    # Class, which returns a list of tuples, and each tuple   #
    # contains a term and its frequency                       #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    def set_frequency(self, tokens):
        self.terms_frequency = Counter(tokens)
        return len(tokens)

    # # # Method to insert the positions of the tokens, beginning in 1. The positions are per token # # #
    def set_positions(self, tokens):
        position = 1
        for token in tokens:
            if token not in self.positions_dict:
                self.positions_dict[token] = [position]
            else:
                self.positions_dict[token].append(position)
            position += 1

    # # # Method to clear the positions dictionary whenever necessary # # #
    def clear_positions_dict(self):
        self.positions_dict.clear()

    # # # Method to create index from terms and postings # # #
    def index_terms(self, doc_id, type_indexer, bm25):
        list_frequencies = []
        list_terms = []
        for t in self.terms_frequency.items():  # iterates through the tuples present in the Counter object
            term = t[0]
            frequency = t[1]
            if type_indexer == 1 or bm25:  # if the indexer is type 1 (unweighted) or if the indexation is with BM25
                posting = [doc_id, frequency]
                if type_indexer == 3:  # if it is the indexer with positions, positions are added to the posting
                    number_positions = len(self.positions_dict[term])
                    posting.append(number_positions)
                    posting.extend(self.positions_dict[term])
                self.add_to_dictionary(term, posting)  # add the term and posting to dictionary
            else:
                list_terms.append(term)  # term is saved for later
                list_frequencies.append(calculations.calculate_tf_idf_weight(frequency))  # calculates weights for
                # each term

        if len(list_frequencies) > 0:  # if this list is populated, it means it has weights, in order of each term
            list_frequencies = calculations.normalize_weights(list_frequencies)  # normalize the weights
            for i in range(0, len(list_frequencies)):  # for each term, get its weight, and create the posting
                term = list_terms[i]
                weight_normalized = list_frequencies[i]
                posting = [doc_id, weight_normalized]  # posting created
                if type_indexer == 3:  # if it is the indexer with positions, positions are added to the posting
                    number_positions = len(self.positions_dict[term])
                    posting.append(number_positions)
                    posting.extend(self.positions_dict[term])
                self.add_to_dictionary(term, posting)  # add the term and posting to dictionary

        self.positions_dict.clear()

    def add_to_dictionary(self, term, posting):
        if term not in self.index_collection:  # if a term is not in the dictionary, create a list with
            # doc_id and the frequency. The reason this is done this way is to reduce memory usage of objects
            # like strings, dictionaries and sets, and because it can be easily extended in the Else statement
            self.index_collection[term] = posting
        else:  # if the term exists in the dictionary, just add both doc_id and its frequency
            self.index_collection[term].extend(posting)

    # # # Merge blocks to multiple files, by size and characters, and finishes indexation # # #
    def heap_merge_index_blocks(self, folder, chunk_size, filename, tokenizer, type_of_indexation, total_docs, bm25):
        current_datetime = datetime.now()  # get current date and time to write to file's name
        dt_string = current_datetime.strftime("_%Y%m%d_%H%M%S")
        is_bm25 = ""
        if bm25:
            is_bm25 = "_bm25"
        output_name = filename + "tkn" + tokenizer + is_bm25 + "/ind" + str(type_of_indexation) + dt_string
        size_in_memory = 0

        # gets all blocks
        blocks = [folder + f for f in listdir(folder) if isfile(join(folder, f)) and f != "readme.txt"]

        with contextlib.ExitStack() as stack:
            files = [stack.enter_context(open(fn)) for fn in blocks]
            aux_list_chars = []  # this list will contain the first letter of the indexes to be written to file
            aux_string_list = []  # this string is going to have all the indexes concatenated to write to file
            idx = ""
            tmp_list = []  # this list is used to hold the postings lists and also extend when duplicates are found

            # the lambda below is used for the heapq.merge to merge only after comparing the terms split by ;. The
            # reason for that is in cases where two blocks have [acid;... acid-something;] and [acid;
            for line in list(heapq.merge(*files, key=lambda s: [st for st in s.partition(' ')[0].split(';', 1) if st])):
                line = line[:-1]  # remove newline
                aux_list = line.split(";")  # split on the separator, so the first one is the index (at this time,
                # without idf, which will be calculated on self.write_index_line), and the rest are the postings
                if aux_list[0] == idx:  # if the file received has duplicated indexes, transform to only one
                    tmp_list.extend(aux_list[1:len(aux_list)].copy())
                    continue

                else:
                    if idx != "":
                        previous_line = self.write_index_line(type_of_indexation, tmp_list, total_docs, idx)  # save
                        # what line was recently written to file

                        size_in_memory += sys.getsizeof(previous_line)
                        aux_string_list.append(previous_line)  # append the completed index line formatted for the
                        # index file

                        # Check if aux_string reached the minimum size the user wants for the index files. At the
                        # next if statement, it checks if the line read from the block has an index starting with a
                        # character present in aux_list_chars. If it is present, the method must continue getting
                        # indexes from the blocks. For example, if aux_string already has more than 50MB,
                        # and chunk_size is 50MB, and it is writing indexes with the character A, it won't write to
                        # file until a different first letter is found, like the character B. This is to ensure that
                        # a query will have to go through only one file for each token.
                        if size_in_memory >= chunk_size:
                            if line[0].upper() not in aux_list_chars:
                                new_filename = output_name + "_" + "_".join(aux_list_chars)
                                with open(new_filename, "w") as f:
                                    f.writelines(aux_string_list)
                                    aux_string_list.clear()
                                    size_in_memory = 0
                                    print("Ficheiro de indices escrito:", new_filename)
                                aux_list_chars.clear()  # after a file is written, this is cleared so new chars can
                                # be read
                        first_char = line[0].upper()

                        # if it's a number, the file has 0-9 in the name
                        if "0-9" not in aux_list_chars and first_char.isdigit():
                            aux_list_chars.append("0-9")
                        elif first_char not in aux_list_chars and not first_char.isdigit():
                            aux_list_chars.append(first_char)  # if not a number, just put it in the list as is (if
                            # it isn't already there)
                    idx = aux_list[0]  # get the index
                    tmp_list = aux_list[1:len(aux_list)].copy()  # get only the postings list
                    continue

            if previous_line.startswith(idx):  # heapq.merge finished reading lines, so now check if the last written
                # line is a duplicate
                tmp_list.extend(aux_list[1:len(aux_list)].copy())
                new_line = self.write_index_line(type_of_indexation, tmp_list, total_docs, idx)
                size_in_memory += sys.getsizeof(new_line)
                aux_string_list.append(new_line)
            else:  # else, just append the index to aux_string
                if idx != "":
                    new_line = self.write_index_line(type_of_indexation, tmp_list, total_docs, idx)
                    size_in_memory += sys.getsizeof(new_line)
                    aux_string_list.append(new_line)

            new_filename = output_name + "_" + "_".join(aux_list_chars)  # last index file
            with open(new_filename, "w") as f:
                f.writelines(aux_string_list)
                print("Ficheiro de indices escrito:", new_filename)
            aux_list_chars.clear()
        print("Total de indices em ficheiros:", self.counter)
        return self.counter

    # # # Method used when SPIMI creates a single block, writes the index with idf and in separate files # # #
    # # # It works similarly to the heap_merge_index_blocks, but with a single file, not worrying about duplicates.
    def write_index_single_blk(self, filename, tokenizer, type_of_indexation, temp_file, total_docs, chunk_size, bm25):
        current_datetime = datetime.now()  # get current date and time to write to file's name
        dt_string = current_datetime.strftime("_%Y%m%d_%H%M%S")
        is_bm25 = ""
        if bm25:
            is_bm25 = "_bm25"
        output_name = filename + "tkn" + tokenizer + is_bm25 + "/ind" + str(type_of_indexation) + dt_string
        input_f = open(temp_file, "r")
        idx = ""
        line = input_f.readline()[:-1]
        aux_list = line.split(";")
        tmp_list = []
        self.counter = 0
        aux_string = ""
        aux_list_chars = []
        while line.rstrip() != '':  # while the SPIMI block hasn't reached its end
            if sys.getsizeof(aux_string) >= chunk_size:  # if the content
                if line[0].upper() not in aux_list_chars:
                    new_filename = output_name + "_" + "_".join(aux_list_chars)
                    with open(new_filename, "w") as f:
                        f.write(aux_string)
                        aux_string = ""
                    aux_list_chars.clear()
            first_char = line[0].upper()
            if "0-9" not in aux_list_chars and first_char.isdigit():
                aux_list_chars.append("0-9")
            elif first_char not in aux_list_chars and not first_char.isdigit():
                aux_list_chars.append(first_char)
            if aux_list[0] == idx:
                tmp_list.extend(aux_list[1:len(aux_list)].copy())
                line = input_f.readline()[:-1]
                aux_list = line.split(";")
                while aux_list[0] == idx:
                    tmp_list.extend(aux_list[1:len(aux_list)].copy())
                    line = input_f.readline()[:-1]
                    aux_list = line.split(";")
            else:
                if idx != "":
                    aux_string += self.write_index_line(type_of_indexation, tmp_list, total_docs, idx)
                idx = aux_list[0]
                tmp_list = aux_list[1:len(aux_list)].copy()
                line = input_f.readline()[:-1]
                aux_list = line.split(";")

        aux_string += self.write_index_line(type_of_indexation, tmp_list, total_docs, idx)
        new_filename = output_name + "_" + "_".join(aux_list_chars)
        with open(new_filename, "w") as f:
            f.write(aux_string)
        aux_list_chars.clear()
        print("Guardando...", self.counter, "indices em ficheiros")

    # # # Method to transform the data into the appropriate format, calculating normalization and idf as well # # # #
    #  The lists docs, weight_list and positions are already ordered, because if tmp_list is like this:             #
    #  ["1:2.0:1,2", "3:1.0:1", "2:3.0:2,3,4"], then after sorted(tmp_list), it will be ordered like:               #
    #  ["1:2.0:1,2", "2:3.0:2,3,4", "3:1.0:1"], and docs will be [1, 2, 3], weight_list will be [2.0, 3.0, 1.0],    #
    #  and positions will be ["1,2", "2,3,4", "1"]. So, they are matched by the index i of the list in the for loop.#
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    def write_index_line(self, type_indexation, tmp_list, total_docs, idx):
        if type_indexation >= 2:  # if the indexation is type 2 or 3
            positions = None
            sorted_list = sorted(tmp_list)  # sort the postings by doc_id, as it is the beginning of each string on
            # tmp_list

            docs = [int(x.split(":")[0]) for x in sorted_list]  # get each doc_id from sorted list and turn it to an int
            weight_list = [float(x.split(":")[1]) for x in sorted_list]  # get each weight, turn it into float

            # if there are three members on this split, it means the posting has positions
            if len(tmp_list[0].split(":")) == 3:
                positions = [x.split(":")[2] for x in sorted_list]  # get the positions for each document
            index_line = idx + ":" + str(calculations.calculate_idf_weight(docs, total_docs))  # calculate idf
            for i in range(0, len(docs)):
                index_line += ";" + str(docs[i]) + ":" + str(weight_list[i])  # for each doc, format
                if positions:  # if there are positions, concatenate
                    index_line += ":" + positions[i]
            self.counter += 1
            return index_line + "\n"

        else:  # if it's the first indexer, just do the same of Assignment 1, without float weights and positions
            sorted_list = sorted(tmp_list, key=lambda x: int(x.split(":")[0]))
            docs = [int(x.split(":")[0]) for x in sorted_list]
            df_list = [int(x.split(":")[1]) for x in sorted_list]
            index_line = idx
            for i in range(0, len(docs)):
                index_line += "," + str(docs[i]) + ":" + str(df_list[i])
            self.counter += 1
            return index_line + "\n"
