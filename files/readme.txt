# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

This folder is for resources used during execution of the project programs, such as the English stop words file,
the queries.txt and the queries.relevance.txt, and any helpful file during tests.