# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #

import os
import psutil
import gc
import Indexer
from os import listdir
from os.path import isfile, join

spimi_folder = "spimiblocks/"
output_folder = "outputfiles/"


def spimi_invert(corpus_obj, token_obj, type_tokenizer, type_indexer, mem_spimi, chunk_size, bm25):
    chunk_size = chunk_size * 1024 * 1024  # transforming to bytes
    delete_blocs(spimi_folder)
    index_obj = None
    mem_process = psutil.Process()
    starting_free_memory = psutil.virtual_memory().free
    starting_process_memory = mem_process.memory_info().rss
    print("SPIMI: Memoria livre no computador ao iniciar:", round(to_megabytes(starting_free_memory)), "MB")
    if isinstance(mem_spimi, float) and mem_spimi > 0:
        memory_block = round(starting_free_memory * mem_spimi / 100.0)
        memory_block = memory_block + starting_process_memory
        if memory_block < mem_process.memory_info().rss:
            raise MemoryError("Porcentagem especificada muito pequena.")
        if memory_block > starting_free_memory:
            raise MemoryError("Porcentagem especificada muito alta.")
    elif isinstance(mem_spimi, int) and mem_spimi > 0:
        memory_block = mem_spimi * 1024 * 1024  # transforming to bytes
        memory_block = memory_block + starting_process_memory
        if memory_block < mem_process.memory_info().rss:
            raise MemoryError("Porcentagem especificada muito pequena.")
        if memory_block > starting_free_memory:
            raise MemoryError("Porcentagem especificada muito alta.")
    else:
        raise MemoryError("Porcentagem especificada menor ou igual a 0, ou nao eh do tipo Float.")
    print("SPIMI: Bloco maximo de memoria a ser utilizado:", round(to_megabytes(memory_block)), "MB")
    print("SPIMI: Processo Python utilizando:", round(to_megabytes(mem_process.memory_info().rss)), "MB")

    # Declaring variables to read corpus #
    doc_id = 0  # id of each document, iterated after CorpusReader.read() returns [PMID, TITLE]
    corpus_gen = corpus_obj.get_info()  # a generator that yields the outputs from CorpusReader.get_info()
    i = 1  # number of blocks, consider it will be at least one
    pmid_reference_file = open(output_folder + "pmids_reference.txt", "w+")
    if bm25:
        doc_length_file = open(output_folder + "doc_lengths.txt", "w+")
    pmid_list = []
    doc_length = []
    doc_length_sum = 0.0

    while True:
        try:
            corpus_elements = []  # auxiliary list to receive the output while checking if they're valid
            while corpus_elements is not None:
                if mem_process.memory_info().rss < memory_block or index_obj is None:
                    if index_obj is None:
                        index_obj = Indexer.Indexer()
                    corpus_elements = next(corpus_gen)
                    pmid_list.append(corpus_elements[0] + "\n")
                    doc_id += 1  # iterates document indexed

                    corpus_elements = corpus_elements[1:]
                    if corpus_elements is not None:

                        # Calls the tokenizer method passed as parameter. The second element of the tuple is the title #
                        if len(corpus_elements) >= 2:
                            field = " ".join(corpus_elements)
                        else:
                            field = corpus_elements[0]
                        tokens = token_obj.tokenize(field, type_tokenizer)

                        if type_indexer == 3:
                            index_obj.set_positions(tokens)

                        # Gets transformed tokens into terms and set their respective frequency in the document #
                        num_terms_in_doc = index_obj.set_frequency(tokens)

                        if bm25:
                            doc_length_sum += num_terms_in_doc
                            doc_length.append(str(num_terms_in_doc) + "\n")

                        tokens.clear()  # reducing memory usage
                        index_obj.index_terms(doc_id, type_indexer, bm25)  # indexation
                else:
                    write_to_block(spimi_folder + "block" + str(i) + "_", type_tokenizer, type_indexer,
                                   index_obj)  # writes all indexes to block
                    i += 1  # increment number of blocks. This turns to 2 the first time it passes by here.
                    # destroy object, set to None, call garbage collector, just to be sure because Python likes to
                    # hold on to allocated memory for future optimization

                    pmid_reference_file.writelines(pmid_list)
                    pmid_list.clear()
                    if bm25:
                        doc_length_file.writelines(doc_length)
                        doc_length.clear()

                    del index_obj
                    index_obj = None
                    gc.collect()
                    print("SPIMI: Processo Python ocupando apos descarregar em bloco:",
                          round(to_megabytes(mem_process.memory_info().rss)), "MB\n")
                    print("Documentos lidos do Corpus:", doc_id)
        except StopIteration:
            break

    pmid_reference_file.writelines(pmid_list)
    pmid_list.clear()
    if bm25:
        doc_length_file.writelines(doc_length)
        avdl = round(doc_length_sum / doc_id, 3)
        doc_length_file.write("Average Document Length: " + str(avdl) + "\n")
        doc_length.clear()

    write_to_block(spimi_folder + "block" + str(i) + "_", type_tokenizer, type_indexer,
                   index_obj)  # writes all indexes to block
    output = ""
    print(doc_id, "obras a serem indexadas.")

    pmid_reference_file.close()

    # if more than one block, finalized indexation and merge them into multiple index files
    if i > 1:
        print("Fazendo merge de", i, "blocos.")
        output = index_obj.heap_merge_index_blocks(spimi_folder, chunk_size, output_folder + "index_", type_tokenizer,
                                                   type_indexer, doc_id, bm25)
        delete_blocs(spimi_folder)  # deletes blocks after merge

    # if only one block, just finalized indexation and change the block to multiple index files
    if output == "":
        print("Apenas um bloco, sem necessidade de merge. Finalizando indexacao.")
        output = spimi_folder + os.listdir(spimi_folder)[0]
        index_obj.write_index_single_blk(output_folder + "index_", type_tokenizer, type_indexer, output,
                                         doc_id, chunk_size, bm25)
        delete_blocs(spimi_folder)  # deletes block


# # # Old way of writing index to disk # # #
def write_to_block(filename, tokenizer, type_of_indexation, index_object):
    print("Guardando...", len(index_object.index_collection), "indices em bloco")
    index_object.index_collection = index_object.sort_index_collection(index_object.index_collection)
    f = open(filename + "tkn" + tokenizer + "_ind" + str(type_of_indexation), "w+")
    for idx in index_object.index_collection:  # gets each index and creates auxiliary variables below
        new_list = []
        aux_count = 0
        aux_string = ""

        # Each list present in the index_collection is connected to an index, and each list has N integers.
        # Every two integers are doc_id and term_frequency. So, every two elements read from the list are
        # concatenated to form doc_id:term_frequency. That's why aux_count and aux_string are reset when
        # aux_count == 2. When aux_count == 2, a new list of doc_id:term_frequency is appended (everything is
        # already ordered). Then, postings receives the list and joins them with ',', so it becomes
        # doc_id_term_frequency,doc_id...
        if type_of_indexation == 1:  # indexing of Assignment 1

            for elem in index_object.index_collection[idx]:  # each index has a list of integers, and elem is each
                # integer
                aux_count += 1
                if aux_count == 2:
                    aux_string += ":" + str(elem)
                    new_list.append(aux_string)
                    aux_count = 0
                    aux_string = ""
                    continue
                aux_string += str(elem)

        elif type_of_indexation == 2:

            i = 0
            for elem in index_object.index_collection[idx]:  # each index has a list of integers, and elem is each
                # integer
                aux_count += 1
                if aux_count == 2:
                    aux_string += ":" + str(elem)
                    i += 1
                    new_list.append(aux_string)
                    aux_count = 0
                    aux_string = ""
                    continue
                aux_string += str(elem)

        elif type_of_indexation == 3:

            positions_list = []
            i = 0
            j = 0
            for elem in index_object.index_collection[idx]:  # each index has a list of integers, and elem is each
                # integer
                aux_count += 1
                if aux_count == 2:
                    aux_string += ":" + str(elem) + ":"
                    i += 1
                    continue
                elif aux_count == 3:
                    j = aux_count + elem
                    continue
                elif aux_count < j:
                    positions_list.append(str(elem))
                    continue
                elif aux_count > 3:
                    positions_list.append(str(elem))
                    j = 0
                    aux_count = 0
                    aux_string += ",".join(positions_list)
                    positions_list.clear()
                    new_list.append(aux_string)
                    aux_string = ""
                    continue

                aux_string += str(elem)

        postings = ";".join(new_list)
        line = idx + ";" + postings + '\n'  # putting the postings after the index as mandated, then newline
        f.write(line)
    f.close()


# # # Transform bytes to MB # # #
def to_megabytes(num_bytes):
    return num_bytes / 1024.0 / 1024.0


# # # Delete the blocks from their folder as they won't be needed anymore # # #
def delete_blocs(folder):
    [os.remove(folder + f) for f in listdir(folder) if f != "readme.txt" and isfile(join(folder, f))]
