import math


# # # Function to calculate the idf of a term # # #
def calculate_idf_weight(number_docs, total_docs):
    df_t = len(number_docs)  # number of documents
    idf_t = round(math.log10(total_docs / df_t), 3)
    return idf_t


# # # Function to calculate the BM25 tf in a document # # #
def calculate_bm25_tf(tf, k1, b, normalized_doc_length):
    numerator = float(tf * (k1 + 1))
    denominator = float(tf + k1 * (1 - b + b * normalized_doc_length))
    return round(numerator/denominator, 3)


# # # Function to calculate the tf-idf of a document # # #
def calculate_tf_idf_weight(frequency):
    return round(1 + math.log10(frequency), 3)  # as in the lnc.ltc schema, document frequency = 1, so tf-idf = tf


# # # Function to calculate the normalization of weights # # #
def normalize_weights(weight_list):
    denominator = 0
    for weight in weight_list:
        denominator += math.pow(weight, 2)
    normalizer = 1.0 / math.sqrt(denominator)
    return [round(w * normalizer, 3) for w in weight_list]  # returns the list of weights already normalized


# # # Function to calculate the Precision # # #
def precision(tp, fp):
    p = tp/(tp + fp)
    return round(p, 3)


# # # Function to calculate the Recall # # #
def recall(tp, fn):
    r = tp/(tp + fn)
    return round(r, 3)


# # # Function to calculate the F-Measure # # #
def f_measure(p, r):
    denominator = (p + r)
    if denominator == 0:
        return 0.0
    f_m = (2 * p * r) / denominator
    return round(f_m, 3)


# # # Function to calculate the Average Precision, by calculating the precision per position on rank # # #
def average_precision(pmid_weight, pmid_relevance):
    tp = 0.0
    rank = 0.0
    result = 0.0
    for pmid in pmid_weight:
        if pmid in pmid_relevance:
            tp += 1.0
            rank += 1.0
            result += tp/rank
        else:
            rank += 1.0
    return round(result/len(pmid_weight), 3)


# Function to calculate the NDCG. First calculates DCG, then sorts the ranks by the most relevant and calculates
# IDCG, which is the ideal DCG (in which the most relevant is ranked number 1, and least relevant is in the last
# position. Then, NDCG is just DCG/IDCG, which gives a number between 0 and 1.
# Most relevant = 2, Least Relevant = 1, Not Relevant (not present in the queries.relevance.txt) = 0.
def NDCG(pmid_weight, pmid_relevance):
    rank_relevance_list = []
    for pmid in pmid_weight:
        if pmid in pmid_relevance:
            rank_relevance_list.append(pmid_relevance[pmid])
        else:
            rank_relevance_list.append(0)

    dcg = 0.0
    for i in range(0, len(rank_relevance_list)):
        if i == 0:
            dcg += rank_relevance_list[i]
            continue
        dcg += rank_relevance_list[i] / math.log2(i + 1)

    idcg = 0.0
    perfect_rank_relevance_list = sorted(rank_relevance_list, reverse=True)  # sorts by most relevant first
    perfect_rank_relevance_list = [rank for rank in perfect_rank_relevance_list if rank != 0]  # removes the 0s for IDCG
    if len(perfect_rank_relevance_list) == 0:
        return 0.0
    for i in range(0, len(perfect_rank_relevance_list)):
        if i == 0:
            idcg += perfect_rank_relevance_list[i]
            continue
        idcg += perfect_rank_relevance_list[i] / math.log2(i + 1)

    normalized = round(dcg/idcg, 3)
    return normalized
