# ----------------------------------------- #
#  Helder Paraense    | N.Mec.: 96307       #
# ----------------------------------------- #
import argparse
import csv
import time
import calculations
from collections import OrderedDict
from RankedRetriever import RankedRetriever
from statistics import median

pmid_references_file = "outputfiles/pmids_reference.txt"  # this file is mandatory
doc_lengths_file = "outputfiles/doc_lengths.txt"  # this file is mandatory

# Utilizing the ArgumentParser to get the arguments to run the script, as well as defining some default values #
parser = argparse.ArgumentParser(description='Script for retrieval of documents present in a corpus',
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-f", default="files/queries.txt", type=str, help="This is the path to the queries file")
parser.add_argument("-fr", default="files/queries.relevance.txt", type=str, help="This is the path to the queries"
                                                                                 " relevance file")
parser.add_argument("-wt", default=0.7, type=float, help="This is the weight threshold for the document frequency.\n"
                                                         "If the weight of a term in a document is below this value,\n"
                                                         "and the term idf is not high enough, it is ignored.")
parser.add_argument("-bm25", default=0, type=int, choices=[0, 1], help="This is for bm25 ranking. The default is "
                                                                       "0:\n0 - Indexed for tf-idf\n1 - Indexed for "
                                                                       "BM25")
parser.add_argument("-k1", default=1.2, type=float, help="This is the k parameter for bm25 ranking.\nThe default is "
                                                         "1.2, and should be a positive real number.")
parser.add_argument("-b", default=0.75, type=float, help="This is the b parameter for bm25 ranking.\nThe default is "
                                                         "0.75, and the value should be between 0.0 and 1.0.\n0.0 "
                                                         "disables normalization, and 1.0 normalizes completely.")
parser.add_argument("-bmt", default=100, type=int, help="This is the documents threshold for the BM25. This is used "
                                                        "as a stop criterium, and indicates \nhow many documents are "
                                                        "going to have their TF calculated before stopping.\n100 is "
                                                        "the default, and it shouldn't be lower than 50 so the test "
                                                        "with 50 retrieved results won't \nhave less than that.\nIf 0 "
                                                        "is given for this argument, the threshold will be ignored.")
parser.add_argument("-sw", default="files/snowball_stopwords_EN.txt", type=str, help="This is the path to the stop "
                                                                                     "words file")
parser.add_argument("-fi", default="outputfiles/index_tknimproved/", type=str, help="This is the path to the indexes "
                                                                                    "folder")

args = parser.parse_args()  # parsing arguments
queries_file = args.f  # gets the queries file
queries_relevance_file = args.fr  # gets the queries relevance file
stop_words_file = args.sw  # gets the stop words file
indexes = args.fi  # gets the path to the indexes
threshold = args.wt  # gets the weight threshold
bm25 = args.bm25  # gets information if the indexes are prepared for bm25 or not

bm25 = True if bm25 == 1 else False
type_index = ""

if bm25:
    bm25_k1 = args.k1  # gets k1 if the bm25 method was used
    if bm25_k1 < 0.0:
        raise ValueError("k1 should be a positive real number.")
    bm25_b = args.b  # gets b if the bm25 method was used
    if bm25_b < 0.0 or bm25_b > 1.0:
        raise ValueError("The b parameter should be a real number between 0 and 1.")
    threshold = args.bmt  # changes weight threshold to the bm25 threshold
    if threshold < 50:
        raise ValueError("The threshold should be higher than 50.")
    thre_s = "Threshold" + str(threshold) if threshold != 0 else "NoThreshold"
    type_index = "bm25_k" + str(bm25_k1) + "_b" + str(bm25_b) + "_" + thre_s + "_"
    retriever = RankedRetriever(indexes, stop_words_file, doc_lengths_file, bm25_k1, bm25_b)
else:
    if threshold < 0.0 or threshold > 1.0:
        raise ValueError("Normalized tf weights are real numbers between 0 and 1, so the wt parameter should be as "
                         "well.")
    type_index = "tf-idf_wt" + str(threshold) + "_"
    retriever = RankedRetriever(indexes, stop_words_file)
num_results = [10, 20, 50]
precisions_list = []
recalls_list = []
f_measures_list = []
average_precisions_list = []
ndcgs_list = []
latency_query_list = []
pmids_score = OrderedDict()
pmids_relevance = OrderedDict()
print()

for n in num_results:
    new_file = "outputfiles/search_results/"
    new_file += type_index + str(n) + "_retrieved.txt"
    n_f = open(new_file, "w+")
    qrf = open(queries_relevance_file, "r")
    rel = qrf.readline()
    rel_split = rel.split("\t")
    i = 0
    for l in open(queries_file):
        query_l = l.split("\t")[1].rstrip()
        docs_list = []
        pmids = []

        print("Query:", query_l)
        n_f.write("Query: " + query_l + "\n")
        start_time = time.time()  # object to calculate at the end the running time of query search
        result = retriever.retrieve(query_l, n, threshold)
        i += 1
        elapsed_time = int(time.time() - start_time)
        latency_query_list.append(elapsed_time % 60)
        print("Pesquisa terminada em " + '{:02d}h{:02d}m{:02d}s'
              .format(elapsed_time // 3600, (elapsed_time % 3600 // 60), elapsed_time % 60))
        with open(pmid_references_file) as pr:
            lines = pr.readlines()
            length = len(lines)
            for r in result:
                docs_list.append(r)
                pmids.append((int(lines[r - 1].rstrip()), round(result[r], 3)))
        for p in pmids:
            n_f.write(str(p[0]) + "\t" + str(p[1]) + "\n")
        n_f.write("\nPesquisa terminada em " + '{:02d}h{:02d}m{:02d}s'
                  .format(elapsed_time // 3600, (elapsed_time % 3600 // 60), elapsed_time % 60) + "\n")

        pmids_score = OrderedDict(pmids)
        print("PMIDs:", pmids_score)

        while int(rel_split[0]) == i:
            pmids_relevance[int(rel_split[1])] = int(1 if rel_split[2].rstrip() == "2" else 2)
            rel = qrf.readline()
            if rel.rstrip() == "":
                break
            rel_split = rel.split("\t")

        tp = 0
        fp = len(pmids_score)
        fn = len(pmids_relevance)
        for pmid in pmids_score:
            if pmid in pmids_relevance:
                tp += 1
                fp -= 1
                fn -= 1

        query_precision = calculations.precision(tp, fp)
        query_recall = calculations.recall(tp, fn)
        query_f_measure = calculations.f_measure(query_precision, query_recall)
        query_average_precision = calculations.average_precision(pmids_score, pmids_relevance)
        query_ndcg = calculations.NDCG(pmids_score, pmids_relevance)

        n_f.write("Precision " + str(query_precision) + "\n")
        n_f.write("Recall " + str(query_recall) + "\n")
        n_f.write("F_Measure " + str(query_f_measure) + "\n")
        n_f.write("Average Precision " + str(query_average_precision) + "\n")
        n_f.write("NDCG " + str(query_ndcg) + "\n\n")

        precisions_list.append(query_precision)
        recalls_list.append(query_recall)
        f_measures_list.append(query_f_measure)
        average_precisions_list.append(query_average_precision)
        ndcgs_list.append(query_ndcg)

        pmids_score.clear()
        pmids_relevance.clear()

        print()

    precision_mean = round(sum(precisions_list) / len(precisions_list), 3)
    recall_mean = round(sum(recalls_list) / len(recalls_list), 3)
    f_measure_mean = round(sum(f_measures_list) / len(f_measures_list), 3)
    query_throughput = round(i / sum(latency_query_list), 2)
    latency_median = round(median(latency_query_list), 2)
    mean_average_precision = round(sum(average_precisions_list) / len(average_precisions_list), 3)
    average_ndcg = round(sum(ndcgs_list) / len(ndcgs_list), 3)

    with open(new_file[:-4] + ".csv", "w+", newline='\n') as csvfile:
        writer = csv.writer(csvfile, delimiter="\t")
        writer.writerow(["Precision", "Recall", "F1", "Avg Prec.", "NDCG", "Latency (s)"])
        columns = [precisions_list, recalls_list, f_measures_list, average_precisions_list, ndcgs_list,
                   latency_query_list]
        rows = zip(*columns)
        writer.writerows(rows)

    n_f.write("Mean Precision" + "\t" + str(precision_mean) + "\n")
    n_f.write("Mean Recall" + "\t" + str(recall_mean) + "\n")
    n_f.write("Mean F_Measure" + "\t" + str(f_measure_mean) + "\n")
    n_f.write("Mean Average Precision" + "\t" + str(mean_average_precision) + "\n")
    n_f.write("Average NDCG" + "\t" + str(average_ndcg) + "\n")
    n_f.write("Query Throughput" + "\t" + str(query_throughput) + " queries per second" + "\n")
    n_f.write("Median Query Latency" + "\t" + str(latency_median) + " seconds" + "\n")
    precisions_list.clear()
    recalls_list.clear()
    f_measures_list.clear()
    latency_query_list.clear()
    average_precisions_list.clear()
    ndcgs_list.clear()
    n_f.close()
    i = 0
