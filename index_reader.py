from collections import OrderedDict
from heapq import merge
from os import listdir
from os.path import isfile, join
import contextlib

# --------------------------------------------------------------
# Informations about the group
# --------------------------------------------------------------
# Dante Marinho     | mec: 83672
# Helder Paraense   | mec: 96307

file = "outputfiles/index_tknimproved_ind3_20191113_142237"


def first_ten_terms(file):
    f = open(file)
    ten_terms_frequency_one = []
    line = f.readline()
    while line:
        string_list = line.split(',')
        if len(string_list) < 3:
            aux = int(string_list[1].split(':')[1])
            if aux == 1:
                ten_terms_frequency_one.append(string_list[0])
            if len(ten_terms_frequency_one) == 10:
                break
        line = f.readline()
    f.close()
    return ten_terms_frequency_one


def terms_most_frequent(file):
    f = open(file)
    ten_terms_most_frequent = {}
    line = f.readline()
    dictionary_of_terms = OrderedDict()
    frequency_list = []
    while line:
        count = 0
        string_list = line.split(',')
        for frequencies in string_list[1:]:
            count += int(frequencies.split(':')[1])
        dictionary_of_terms[count] = string_list[0]
        frequency_list.append(count)
        line = f.readline()
    f.close()

    while len(ten_terms_most_frequent) != 10:
        m = max(frequency_list)
        idx = frequency_list.index(m)
        frequency_list[idx] = 0
        ten_terms_most_frequent[dictionary_of_terms[m]] = m

    return ten_terms_most_frequent


def count_first_character(file):
    f = open(file)
    line = f.readline()
    dictionary = {}
    while line:
        if line[0].isdigit():
            if "number" not in dictionary:
                dictionary["number"] = 0
            dictionary["number"] += 1
        elif line[0] == 'a':
            if "A" not in dictionary:
                dictionary["A"] = 0
            dictionary["A"] += 1
        elif line[0] == 'b':
            if "B" not in dictionary:
                dictionary["B"] = 0
            dictionary["B"] += 1
        elif line[0] == 'c':
            if "C" not in dictionary:
                dictionary["C"] = 0
            dictionary["C"] += 1
        elif line[0] == 'd':
            if "D" not in dictionary:
                dictionary["D"] = 0
            dictionary["D"] += 1
        elif line[0] == 'e':
            if "E" not in dictionary:
                dictionary["E"] = 0
            dictionary["E"] += 1
        elif line[0] == 'f':
            if "F" not in dictionary:
                dictionary["F"] = 0
            dictionary["F"] += 1
        elif line[0] == 'g':
            if "G" not in dictionary:
                dictionary["G"] = 0
            dictionary["G"] += 1
        elif line[0] == 'h':
            if "H" not in dictionary:
                dictionary["H"] = 0
            dictionary["H"] += 1
        elif line[0] == 'i':
            if "I" not in dictionary:
                dictionary["I"] = 0
            dictionary["I"] += 1
        elif line[0] == 'j':
            if "J" not in dictionary:
                dictionary["J"] = 0
            dictionary["J"] += 1
        elif line[0] == 'k':
            if "K" not in dictionary:
                dictionary["K"] = 0
            dictionary["K"] += 1
        elif line[0] == 'l':
            if "L" not in dictionary:
                dictionary["L"] = 0
            dictionary["L"] += 1
        elif line[0] == 'm':
            if "M" not in dictionary:
                dictionary["M"] = 0
            dictionary["M"] += 1
        elif line[0] == 'n':
            if "N" not in dictionary:
                dictionary["N"] = 0
            dictionary["N"] += 1
        elif line[0] == 'o':
            if "O" not in dictionary:
                dictionary["O"] = 0
            dictionary["O"] += 1
        elif line[0] == 'p':
            if "P" not in dictionary:
                dictionary["P"] = 0
            dictionary["P"] += 1
        elif line[0] == 'q':
            if "Q" not in dictionary:
                dictionary["Q"] = 0
            dictionary["Q"] += 1
        elif line[0] == 'r':
            if "R" not in dictionary:
                dictionary["R"] = 0
            dictionary["R"] += 1
        elif line[0] == 's':
            if "S" not in dictionary:
                dictionary["S"] = 0
            dictionary["S"] += 1
        elif line[0] == 't':
            if "T" not in dictionary:
                dictionary["T"] = 0
            dictionary["T"] += 1
        elif line[0] == 'u':
            if "U" not in dictionary:
                dictionary["U"] = 0
            dictionary["U"] += 1
        elif line[0] == 'v':
            if "V" not in dictionary:
                dictionary["V"] = 0
            dictionary["V"] += 1
        elif line[0] == 'w':
            if "W" not in dictionary:
                dictionary["W"] = 0
            dictionary["W"] += 1
        elif line[0] == 'x':
            if "X" not in dictionary:
                dictionary["X"] = 0
            dictionary["X"] += 1
        elif line[0] == 'y':
            if "Y" not in dictionary:
                dictionary["Y"] = 0
            dictionary["Y"] += 1
        elif line[0] == 'z':
            if "Z" not in dictionary:
                dictionary["Z"] = 0
            dictionary["Z"] += 1
        line = f.readline()
    f.close()
    return [x for x in dictionary.items()]


# result = first_ten_terms(file)
# print("Os 10 primeiros termos que aparecem apenas uma vez:", result)
#
# result = terms_most_frequent(file)
# print("Os 10 termos que mais aparecem:", result)

#result = count_first_character(file)
#print("Os 10 termos que mais aparecem:", result)

# folder = "spimiblocks/"
#
# section = lambda s: [st for st in s.partition(' ')[0].split(';',1) if st]
# blocks = [folder + f for f in listdir(folder) if isfile(join(folder, f))]
# with contextlib.ExitStack() as stack, open('output2.txt', 'w') as output_file:
#     files = [stack.enter_context(open(chunk)) for chunk in blocks]
#     output_file.writelines(list(merge(*files, key=section)))
#
# one = ["acid", "acid-lib", "acid-man"]
# two = ["aci-1", "acid", "acid-bind"]
#
# print(list(merge(one, two)))
positions = ["1,2", "2,3,4", "1"]
index_line = ["aaa:1.0;1:1.0", "aab:1.0;1:1.0", "aac:1.0;1:1.0"]
for i in range(0, len(positions)):
    output = index_line[i] + ":" + positions[i]
    print(output)
